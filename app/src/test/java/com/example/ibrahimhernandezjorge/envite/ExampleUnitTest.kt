package com.example.ibrahimhernandezjorge.envite

import com.example.ibrahimhernandezjorge.envite.cartas.Baraja
import com.example.ibrahimhernandezjorge.envite.cartas.Carta
import com.example.ibrahimhernandezjorge.envite.cartas.Palo
import org.junit.Before
import org.junit.Test

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    private val baraja = Baraja(4)
    private val perica = Carta(10, baraja.oro)
    private val caballo = Carta(11, baraja.bastos)
    private val tresDeBastos = Carta(3, baraja.bastos)
    private var cartaVirada = Carta(4, Palo(Utils.ORO))


    @Test
    fun isTriunfo() {
        for (triunfo in baraja.triunfos)
            println("${triunfo.numero} de ${triunfo.palo}")
        println()

        println("Categoría Perica: ${baraja.getCategoriaTriunfo(perica)}")
        println("Categoría Caballo: ${baraja.getCategoriaTriunfo(caballo)}")
        println("Categoría Tres de Bastos: ${baraja.getCategoriaTriunfo(tresDeBastos)}")
    }

    @Test
    fun getGanador() {
        val cartas = HashMap<Int, Carta>()

        cartas[0] = Carta(3, Palo(Utils.COPAS))
        cartas[2] = Carta(4, Palo(Utils.BASTOS))
        cartas[4] = Carta(7, Palo(Utils.ESPADAS))
        cartas[5] = Carta(3, Palo(Utils.ESPADAS))
        cartas[6] = Carta(2, Palo(Utils.ESPADAS))
        cartas[7] = Carta(10, Palo(Utils.BASTOS))
        cartas[8] = Carta(1, Palo(Utils.ESPADAS))
        cartas[12] = Carta(2, Palo(Utils.COPAS))
        cartas[9] = Carta(12, Palo(Utils.ESPADAS))
        cartas[10] = Carta(11, Palo(Utils.ESPADAS))
        cartas[13] = Carta(1, Palo(Utils.COPAS))
        cartas[11] = Carta(3, Palo(Utils.ESPADAS))
        cartas[14] = Carta(12, Palo(Utils.COPAS))

        println("Posición jugador ganador: ${baraja.getPosicionGanadora(cartas, cartaVirada)}")
    }

    @Test
    fun puntacionGanaTest() {
        val puntuacion = Puntuacion()

        puntuacion.addPiedras(0)
        puntuacion.addPiedras(0)
        puntuacion.addPiedras(1)
        puntuacion.addPiedras(0)
        puntuacion.addPiedras(0)
        puntuacion.addPiedras(0)
        puntuacion.addPiedras(0)
        puntuacion.addPiedras(0)
        //Chico equipo 1
        puntuacion.addPiedras(1)
        puntuacion.addPiedras(1)
        puntuacion.addPiedras(1)
        puntuacion.addPiedras(1)
        puntuacion.addPiedras(1)
        puntuacion.addPiedras(1)
        puntuacion.addPiedras(1)
        //Chico equipo 2
        puntuacion.addPiedras(1)
        puntuacion.addPiedras(1)
        puntuacion.addPiedras(1)
        puntuacion.addPiedras(1)
        puntuacion.addPiedras(1)
        puntuacion.addPiedras(1)
        puntuacion.addPiedras(1)
        //Partido equipo 2
        puntuacion.addPiedras(1)
        puntuacion.addPiedras(1)
        puntuacion.addPiedras(1)
        puntuacion.addPiedras(1)
        puntuacion.addPiedras(1)
        puntuacion.addPiedras(1)
        puntuacion.addPiedras(1)
        puntuacion.addPiedras(1)
        puntuacion.addPiedras(1)
        puntuacion.addPiedras(1)
        puntuacion.addPiedras(1)
        puntuacion.addPiedras(1)
        puntuacion.addPiedras(1)
        puntuacion.addPiedras(1)

        println("Puntuación de Equipo 1: ${puntuacion.piedras[0]}     Chicos Equipo 1: ${puntuacion.chicos[0]}     Partidos Equipo 1: ${puntuacion.partidos[0]}")
        println("Puntuación de Equipo 2: ${puntuacion.piedras[1]}     Chicos Equipo 2: ${puntuacion.chicos[1]}     Partidos Equipo 2: ${puntuacion.partidos[1]}")
    }

    @Test
    fun puntuacionTumboTest() {
        val puntuacion = Puntuacion()

        puntuacion.addPiedras(1)
        puntuacion.addPiedras(1)
        puntuacion.addPiedras(0)
        puntuacion.addPiedras(1)
        puntuacion.addPiedras(1)
        puntuacion.addPiedras(1)
        puntuacion.addPiedras(0)
        puntuacion.addPiedras(1)
        puntuacion.addPiedras(0)
        puntuacion.addPiedras(0)
        puntuacion.addPiedras(0)
        puntuacion.addPiedras(0)

        println("Puntuación de Equipo 1: ${puntuacion.piedras[0]}     Chicos Equipo 1: ${puntuacion.chicos[0]}     Partidos Equipo 1: ${puntuacion.partidos[0]}")
        println("Puntuación de Equipo 2: ${puntuacion.piedras[1]}     Chicos Equipo 2: ${puntuacion.chicos[1]}     Partidos Equipo 2: ${puntuacion.partidos[1]}")
    }

    @Test
    fun jugadaIA() {
        val main = MainActivity()
        main.nuevaPartida()
        println("Carta Virada:  ${main.cartaVirada}")

        for(i in 0 until main.numeroJugadores) {
            if(i % 2 == 0)
                print("Jugador ${i + 1}: ")
            else
                print("Jugador IA ${i + 1}: ")
            for (j in 0 until main.cartasJugadores.size - 1)
                print("${main.cartasJugadores[i][j]}     ")
            println()
        }
        println()

       for (cartas in main.jugadorIA.cartasEquipo) {
           for (carta in cartas)
               print("$carta   ")
           println()
       }
        main.nuevoTurno()
        main.nuevoTurno()
        main.getJugadaIA()
    }
}
