package com.example.ibrahimhernandezjorge.envite.cartas

class Palo(var nombre: String) {
    override fun toString(): String {
        return nombre
    }

    override fun equals(other: Any?): Boolean {
        val otroPalo = other as Palo

        return this.nombre == otroPalo.nombre
    }
}