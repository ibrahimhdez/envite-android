package com.example.ibrahimhernandezjorge.envite

class Puntuacion {
    lateinit var piedras: ArrayList<Int>
    lateinit var chicos: ArrayList<Int>
    lateinit var partidos: ArrayList<Int>
    var puntosMano = 2
    var tumbo: Int? = null
    private var ambosTumbo = false

    init {
        initPartida()
    }

    private fun initPartida() {
        piedras = initArray()
        chicos = initArray()
        partidos = initArray()
    }

    private fun initArray(): ArrayList<Int> {
        val array = ArrayList<Int>(2)
        array.add(0)
        array.add(0)

        return array
    }

    fun addPiedras(numEquipo: Int, tumboAnulado: Boolean = false) {
        if(tumbo != null && !tumboAnulado)
            addPiedrasTumbo(numEquipo)
        else {
            var nuevaPuntuacion = piedras[numEquipo] + puntosMano

            when {
                nuevaPuntuacion == 12 -> nuevaPuntuacion = 11
                nuevaPuntuacion >= 12 -> addChico(numEquipo)
            }

            piedras[numEquipo] = nuevaPuntuacion

            if (piedras[numEquipo] == 11)
                irATumbo(numEquipo)
        }

        if(piedras[numEquipo] == 10)
            puntosMano = 1
        else
            resetPuntosMano()
    }

    fun addPiedras(numEquipo: Int, numPuntos: Int, tumboAnulado: Boolean = false) {
        puntosMano = numPuntos

        addPiedras(numEquipo, tumboAnulado)
    }

    private fun addChico(numEquipo: Int) {
        piedras = initArray()
        chicos[numEquipo] = chicos[numEquipo] + 1

        if(chicos[numEquipo] == 2)
            addPartido(numEquipo)
    }

    private fun addPartido(numEquipo: Int) {
        chicos = initArray()
        partidos[numEquipo] = partidos[numEquipo] + 1

        if(partidos[numEquipo] == 2)
            println("GANA EL EQUIPO: ${numEquipo + 1}")
    }

    private fun irATumbo(numEquipo: Int) {
        if(tumbo != null)
            ambosTumbo = true
        else
            tumbo = numEquipo
    }

    private fun addPiedrasTumbo(numEquipo: Int) {
        if(tumbo == numEquipo || ambosTumbo) {
            addChico(numEquipo)
            tumbo = null
            ambosTumbo = false
        }
        else
            addPiedras(numEquipo, 3, true)
    }

    private fun resetPuntosMano() {
        puntosMano = 2
    }

    fun envido() {
        when(puntosMano) {
            1, 2 -> puntosMano += 2
            3, 4 -> puntosMano = 7
            7 -> puntosMano = 9
            //TODO chico
            //TODO partido
        }
    }
}