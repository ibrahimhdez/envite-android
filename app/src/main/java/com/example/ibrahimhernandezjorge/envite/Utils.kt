package com.example.ibrahimhernandezjorge.envite

object Utils {
    const val ORO = "oro"
    const val COPAS = "copas"
    const val ESPADAS = "espadas"
    const val BASTOS = "bastos"
    const val TRIUNFO_CAT_1 = "1"
    const val TRIUNFO_CAT_2 = "2"
    const val TRIUNFO_CAT_3 = "3"
    const val TRIUNFO_CAT_4 = "4"
    const val TRIUNFO_CAT_5 = "5"
}