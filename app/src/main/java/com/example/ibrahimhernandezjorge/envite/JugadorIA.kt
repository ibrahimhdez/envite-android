package com.example.ibrahimhernandezjorge.envite

import com.example.ibrahimhernandezjorge.envite.cartas.Baraja
import com.example.ibrahimhernandezjorge.envite.cartas.Carta

class JugadorIA(var cartasEquipo: ArrayList<ArrayList<Carta>>, var baraja: Baraja) {
    var cartasJugadasRival = ArrayList<ArrayList<Carta>>()
    var categoriaTriunfosEquipo = ArrayList<String>()
    var categoriaTriunfosTiradas = ArrayList<String>()
    var jugadoresConTriunfo = ArrayList<Int>()
    var partidaGanada = false
    private var cartasJugadas = ArrayList<Carta>()
    private lateinit var cartaVirada: Carta

    /**
     * Método que decidirá que carta jugará la IA
     * @param numeroManos Contendrá el número de manos que se han jugado en la partida actual
     * @param numeroJugador Indicará si es el número del jugador dentro del equipo
     * @param turnoTotal Llevará la cuenta de cartas que se han jugado en la mano (para saber si se trata por ejemplo del primer jugador de la mano)
     * @param cartasRival Cartas del equipo contrario (en este caso el del jugador humano)
     * @param puntosPartida Puntos que lleva la IA en la partida actual
     */
    fun juegaCarta(numeroManos: Int, numeroJugador: Int, turnoTotal: Int, cartasRival: ArrayList<ArrayList<Carta>>, puntosPartida: Int, cartasManoActual: HashMap<Int, Carta>): Carta? {
        var carta: Carta? = null
        if(!partidaGanada) {
            categoriaTriunfosTiradas.clear()
            //getCategoriaTriunfosTirados(cartasJugadas)
            carta = comprobarSituacionActual(numeroManos, numeroJugador, turnoTotal, cartasRival, puntosPartida, cartasManoActual)
        }

        else
            println("Partida ganada por IA")


        val cartasJugador = cartasEquipo[numeroJugador]


        eliminarCartaJugada(carta)
        return carta
    }

    private fun eliminarCartaJugada(carta: Carta?) {
        if(carta != null)
            cartasJugadas.add(carta)
    }

    fun comprobarCartasInicio(cartaVirada: Carta) {
        this.cartaVirada = cartaVirada

        getCategoriaTriunfosInicio()
        comprobarTriunfosGanadores()
    }

    private fun getCategoriaTriunfosInicio() {
        var numeroJugador = -1

        for(cartas in cartasEquipo) {
            numeroJugador = numeroJugador.inc()

            for (carta in cartas) {
                if (baraja.isTriunfo(carta)) {
                    val categoriaTriunfo = baraja.getCategoriaTriunfo(carta)

                    jugadoresConTriunfo.add(numeroJugador)
                    categoriaTriunfosEquipo.add(categoriaTriunfo)
                }
            }
        }
    }

    private fun getCategoriaTriunfosTirados(cartasTiradas: ArrayList<Carta>) {
        for(carta in cartasTiradas) {
            if(baraja.isTriunfo(carta)) {
                val categoriaTriunfo = baraja.getCategoriaTriunfo(carta)

                categoriaTriunfosTiradas.add(categoriaTriunfo)
            }
        }
    }

    private fun comprobarSituacionActual(numeroManos: Int, numeroJugador: Int, turno: Int, cartasRival: ArrayList<ArrayList<Carta>>, puntosIA: Int, cartasManoActual: HashMap<Int, Carta>): Carta? {
        var cartaAJugar: Carta? = null
        val mayoresCartas = baraja.getMayoresCartas(cartasEquipo, cartasRival)

        comprobarSiPartidaGanada(mayoresCartas, puntosIA)

        if(!partidaGanada) {
            if(numeroJugador == 0) {
                cartaAJugar = analizaPrimerJugador(numeroManos, puntosIA)
            }
            else if(numeroJugador == 1) {
                cartaAJugar = analizaSegundoJugador(cartasManoActual)
            }
        }
        else {
            //TODO Implementar jugada teniendo ganada la partida
            println("IA DICE: LA PARTIDA ESTA GANADA, NO SE QUE HACER.")
        }


        return cartaAJugar
    }

    /**
     * Comprueba a partir de los puntos que tiene la IA y las mayores cartas que quedan por jugar si ya tenemos la partida ganada
     */
    private fun comprobarSiPartidaGanada(mayoresCartas: ArrayList<Carta>, puntosIA: Int) {
        val cartasEquipoArray = ArrayList<Carta>()

        //Unificar todas las cartas del equipo en un solo array
        for(cartas in cartasEquipo)
            for(carta in cartas)
                cartasEquipoArray.add(carta)

        //Comprobar si partida ganada por méritos propios (sin utilizar carta virada)
        //Si se tiene en posesión los dos mayores cartas posibles, partida ganada
        if(cartasEquipoArray.contains(mayoresCartas[0]) && cartasEquipoArray.contains(mayoresCartas[1]))
            partidaGanada = true
        //Si se tiene en posesión la mayor carta posible y ya se ha ganado una mano, partida ganada
        else if(cartasEquipoArray.contains(mayoresCartas[0]) && puntosIA == 1)
            partidaGanada = true

        //Comprobar si partida ganada con ayuda de carta virada
        //Si carta virada es la segunda mayor y se tiene en posesion la primera y la segunda mayores cartas, partida ganada
        else if(cartaVirada == mayoresCartas[1] && cartasEquipoArray.contains(mayoresCartas[0]) && cartasEquipoArray.contains(mayoresCartas[2]))
            partidaGanada = true
        //Si carta virada es la mayor y se tiene en posesion la segunda y la tercera mayores cartas, partida ganada
        else if(cartaVirada == mayoresCartas[0] && cartasEquipoArray.contains(mayoresCartas[1]) && cartasEquipoArray.contains(mayoresCartas[2]))
            partidaGanada = true
        //Si ya se ha ganada una mano y la mayor es la virada y tenemos la segunda, partida ganada
        else if(cartaVirada == mayoresCartas[0] && cartasEquipoArray.contains(mayoresCartas[1]) && puntosIA == 1)
            partidaGanada = true
    }

    /**
     * Método que analizará para ver qué carta jugar el primer jugador
     * @param numeroManos Número de manos que lleva la partida
     * @param puntosIA Puntos que tiene el jugador IA
     * @return Carta que jugará
     */
    private fun analizaPrimerJugador(numeroManos: Int, puntosIA: Int): Carta? {
        val jugador = 0
        var cartaAJugar: Carta? = null
        val cartasAJugar = ArrayList<Carta>()

        for (carta in cartasEquipo[jugador])
            if(!cartasJugadas.contains(carta))
                cartasAJugar.add(carta)

        //Si es la primera mano, jugaremos la peor carta a no ser que sea imposible (flus)
        if(numeroManos == 0) {
            val peorCarta = baraja.getPeorCarta(cartasAJugar)

            if (peorCarta == null) {
                //TODO tiene que arrastrar
            } else {
                cartaAJugar = peorCarta
            }
        }

        return cartaAJugar
    }

    /**
     * Método que analizará para ver qué carta jugar el primer jugador
     * @param numeroManos Número de manos que lleva la partida
     * @param puntosIA Puntos que tiene el jugador IA
     * @return Carta que jugará
     */
    private fun analizaSegundoJugador(cartasManoActual: HashMap<Int, Carta>): Carta? {
        val jugador = 1
        var cartaAJugar: Carta?
        val cartasAJugar = ArrayList<Carta>()

        for (carta in cartasEquipo[jugador])
            if(!cartasJugadas.contains(carta))
                cartasAJugar.add(carta)

        if(vaGanando(cartasManoActual)) {
            cartaAJugar = baraja.getPeorCarta(cartasAJugar)
        }
        else {
            cartaAJugar = cartaGanaMano(cartasAJugar, cartasManoActual)

            if(cartaAJugar == null) {
                cartaAJugar = baraja.getPeorCarta(cartasAJugar)
            }
        }

        return cartaAJugar
    }

    private fun vaGanando(cartasManoActual: HashMap<Int, Carta>): Boolean {
        var vaGanando = false
        val posicionCartaGanadoraActual = baraja.getPosicionGanadora(cartasManoActual)

        if(cartasManoActual.size % 2 == 0) {
            if(posicionCartaGanadoraActual % 2 == 0)
                vaGanando = true
        }
        else {
            if (posicionCartaGanadoraActual % 2 == 1)
                vaGanando = true
        }

        return vaGanando
    }

    private fun cartaGanaMano(cartas: ArrayList<Carta>, cartasManoActual: HashMap<Int, Carta>): Carta? {
        var cartaGanadora: Carta? = null

        //TODO optimizar para ganar con la carta más baja o echar la peor carta en caso de que no se pueda ganar
        for (carta in cartas) {
            if(cartaGanadora == null) {
                cartasManoActual[999] = carta
                val pos = baraja.getPosicionGanadora(cartasManoActual)

                if (pos == cartasManoActual.size - 1) {
                    cartaGanadora = carta
                }
                else {
                    cartasManoActual.remove(999)
                }
            }
        }

        return cartaGanadora
    }

    /**
     * Método para comprobar si al empezar la partida el equipo ya ha ganado por las cartas que tiene
     * @param cartaVirada Carta que está virada en el tablero
     */
    private fun comprobarTriunfosGanadores() {
        //Si el equipo tiene los dos mayores triunfos, la partida está ganada
        if(categoriaTriunfosEquipo.contains(Utils.TRIUNFO_CAT_1) && categoriaTriunfosEquipo.contains(Utils.TRIUNFO_CAT_2))
            partidaGanada = true

        //Comprobamos si la carta virada es el triunfo mayor
        else if(baraja.getCategoriaTriunfo(cartaVirada) == Utils.TRIUNFO_CAT_1)
        //Si la carta virada es el triunfo mayor y el equipo tiene el segundo y tercer triunfo, la partida está ganada
            if(categoriaTriunfosEquipo.contains(Utils.TRIUNFO_CAT_2) && categoriaTriunfosEquipo.contains(Utils.TRIUNFO_CAT_3))
                partidaGanada = true

            //Comprobamos si la carta virada es el segundo triunfo
            else if(baraja.getCategoriaTriunfo(cartaVirada) == Utils.TRIUNFO_CAT_2)
            //Si está virado el segundo triunfo y el equipo tiene el primer y tercer triunfo, la partida está ganada
                if(categoriaTriunfosEquipo.contains(Utils.TRIUNFO_CAT_1) && categoriaTriunfosEquipo.contains(Utils.TRIUNFO_CAT_3))
                    partidaGanada = true

    }
}