package com.example.ibrahimhernandezjorge.envite.cartas


class Carta(var numero: Int, var palo: Palo) {
    var idImagen = "${palo}_$numero"

    override fun equals(other: Any?): Boolean {
        val otraCarta = other as Carta

        return (this.numero == otraCarta.numero) && (this.palo == otraCarta.palo)
    }

    override fun hashCode(): Int {
        var result = numero
        result = 31 * result + palo.hashCode()
        result = 31 * result + (idImagen.hashCode())
        return result
    }

    override fun toString(): String {
        return "$numero de $palo"
    }
}