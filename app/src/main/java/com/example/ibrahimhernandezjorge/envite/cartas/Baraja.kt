package com.example.ibrahimhernandezjorge.envite.cartas

import android.graphics.Bitmap
import com.example.ibrahimhernandezjorge.envite.Utils
import android.graphics.BitmapFactory
import kotlin.collections.HashMap
import kotlin.collections.ArrayList


class Baraja(var numeroJugadores: Int) {
    var oro = Palo(Utils.ORO)
    var copas = Palo(Utils.COPAS)
    var espadas = Palo(Utils.ESPADAS)
    var bastos = Palo(Utils.BASTOS)
    var triunfos = ArrayList<Carta>()
    val perica = Carta(10, oro)
    val caballo = Carta(11, bastos)
    val tresDeBastos = Carta(3, bastos)
    private val cincoOro = Carta(5, oro)
    private val unoOro = Carta(1, oro)

    private var palos = arrayOf(oro, copas, espadas, bastos).toCollection(ArrayList())
    private var numeroCartas = intArrayOf(1, 2, 3, 4, 5, 6, 7, 10, 11, 12).toCollection(ArrayList())
    var cartas = ArrayList<Carta>()
    var cartaVirada: Carta? = null

    init {
        initCartas()
        initTriunfos()
    }

    private fun initCartas() {
        val options = BitmapFactory.Options()
        options.inPreferredConfig = Bitmap.Config.ARGB_8888

        for(palo in palos) {
            for(numero in numeroCartas) {
                val carta = Carta(numero, palo)

                //carta.imagen = bitmap
                cartas.add(carta)
            }
        }
    }

    fun virarCarta(): Carta {
        cartaVirada = getCartaRandom()

        cartas.remove(cartaVirada!!)

        return cartaVirada!!
    }

    /**
     * Método que inicia los triunfos dependiendo del número de jugadores que estén jugando
     */
    private fun initTriunfos() {
        triunfos.add(perica)
        triunfos.add(caballo)
        triunfos.add(tresDeBastos)

        when(numeroJugadores) {
            //Si la partida es de 8 jugadores o más, añadimos el 5 de oro como triunfo
            8 ->
                triunfos.add(cincoOro)

            //Si la partida es de 10 jugadores, también añadimos el 1 de oro como triunfo
            10 -> {
                triunfos.add(cincoOro)
                triunfos.add(unoOro)
            }
        }
    }

    /**
     * Método que reparte 3 cartas al azar a un jugador
     *
     * @return ArrayList de objetos Carta que contendrá las 3 cartas del jugador
     */
    fun getTresCartas(): ArrayList<Carta> {
        val tresCartas = ArrayList<Carta>()

        repeat(3) {
            val carta = getCartaRandom()

            tresCartas.add(carta)
            cartas.remove(carta)
        }

        return tresCartas
    }

    private fun getCartaRandom(): Carta {
        val randomInt = (0..(cartas.size - 1)).shuffled().first()

        return cartas[randomInt]
    }

    /**
     * Método que devuelve las 3 mayores cartas posibles (sin contar cartas de tu equipo y carta virada)
     * @param cartasRival Array de cartas del rival para mezclarlas con las cartas del montón
     * @return Array con las 3 mayores cartas posibles
     */
    fun getMayoresCartas(cartasEquipo: ArrayList<ArrayList<Carta>>, cartasRival: ArrayList<ArrayList<Carta>>): ArrayList<Carta> {
        val cartas = ArrayList<Carta>()
        cartas.addAll(this.cartas)
        val mayoresCartas = ArrayList<Carta>()

        //Añadir cartas de los equipos a las cartas del montón
        for(cartasJugadorRival in cartasRival)
            for(carta in cartasJugadorRival)
                cartas.add(carta)
        for(cartasJugadorEquipo in cartasEquipo)
            for(carta in cartasJugadorEquipo)
                cartas.add(carta)

        if(numeroJugadores == 8) {
            if (cartas.contains(cincoOro))
                mayoresCartas.add(cincoOro)
        }
        else if(numeroJugadores == 10)
            if(cartas.contains(unoOro))
                mayoresCartas.add(unoOro)

        if(cartas.contains(tresDeBastos))
            mayoresCartas.add(tresDeBastos)

        if(mayoresCartas.size < 3) {
            if (cartas.contains(caballo))
                mayoresCartas.add(caballo)
            if (cartas.contains(perica))
                mayoresCartas.add(perica)
        }

        if(mayoresCartas.size < 3) {
            val cartasPalo = this.cartas.filter { carta -> carta.palo == cartaVirada!!.palo } as ArrayList<Carta>
            val cartasPaloOrdenadas = ordenarPaloPorValor(cartasPalo)

            while(mayoresCartas.size < 3)
                mayoresCartas.add(cartasPaloOrdenadas.removeAt(0))
        }

        return mayoresCartas
    }

    /**
     * Método que ordena por valor cartas del mismo palo teniendo en cuenta la carta que está virada
     * @param cartas Cartas del mismo palo que se van a ordenar
     * @return Array con las cartas ordenadas, donde la de indice menor es de mayor valor
     */
    private fun ordenarPaloPorValor(cartas: ArrayList<Carta>): ArrayList<Carta> {
        val palo = cartas[0].palo
        val cartasOrdenadas = ArrayList<Carta>()

        //Primero comprobamos si el palo es de lo virado y si existe la malilla
        if((palo == cartaVirada!!.palo) && (cartas.contains(Carta(2, palo)))) {
            val malilla = cartas.filter { carta -> carta.numero == 2 }[0]

            cartasOrdenadas.add(malilla)
            cartas.remove(malilla)
        }

        for(x in cartas.size downTo 1) {
            val carta = cartas[x - 1]

            //Si vamos a incluir la carta número 7 y también existe el 1, metemos primero el 1
            if(carta.numero == 7 && cartas.contains(Carta(1, palo))) {
                val uno = cartas.filter { card -> card.numero == 1 }[0]

                cartasOrdenadas.add(uno)
                cartas.remove(uno)
            }

            if(cartasOrdenadas.size == 3)
                break
            else {
                cartasOrdenadas.add(carta)
                cartas.remove(carta)
            }
        }

        return cartasOrdenadas
    }

    /**
     * Método que comprueba si una carta es un triunfo
     *
     * @param carta Carta que comprobaremos
     * @return True si es triunfo, false si no
     */
    fun isTriunfo(carta: Carta): Boolean {
        return triunfos.contains(carta)
    }

    /**
     * Método que dado las cartas de un equipo te devuelve la peor
     */
    fun getPeorCarta(cartas: ArrayList<Carta>): Carta? {
        var peoresCartas = cartas.filter { carta -> !isTriunfo(carta) }
        peoresCartas = peoresCartas.filter { carta -> carta.palo != cartaVirada!!.palo }

        return if(peoresCartas.isNotEmpty())
            peoresCartas[0]
        else {
            //TODO Ordenar teniendo en cuenta el 1
            val cartasOrdenadas = cartas.sortedBy { it.numero }

            if (!cartasOrdenadas.isEmpty()) {
                cartasOrdenadas.first()
            }
            else
                null
        }
    }

    private fun selector(carta: Carta): Int = carta.numero

    /**
     * Método que devolverá la posición de la carta que ha ganado la mano
     *
     * @param cartas Hash que contendrá las posiciones de los jugadores y la carta que ha jugado
     * @param cartaVirada Carta que está virada en la mesa para comprobarla carta ganadora
     * @return Posición del jugador que ha ganado la mano
     */
    fun getPosicionGanadora(cartas: HashMap<Int, Carta>): Int {
        val jugadoresConTriunfo = getPosTriunfos(cartas)

        //Si algunos jugadores jugaron triunfos, estos serán los ganadores
        when(jugadoresConTriunfo.size) {
            //Si el array tiene tamaño 0 quiere decir que ningún jugador ha jugado un triunfo
            0 -> {
                //Comprobamos la mayo
                return getPosMayorCarta(cartas, cartaVirada!!)
            }

            1 -> {
                //Si solo se ha jugado un triunfo, este es el ganador de la mano
                return jugadoresConTriunfo[0]
            }

            //Si se han jugado más de un triunfo, se procede a comprobar cuál es el mayor
            else -> {
                val hashPosTriunfos = HashMap<Int, Carta>()

                //Sacamos del Hash las jugadas que corresponden únicamente a los triunfos
                for(pos in jugadoresConTriunfo)
                    hashPosTriunfos[pos] = cartas[pos]!!

                return getPosMayorTriunfo(hashPosTriunfos)
            }
        }
    }

    private fun getPosMayorCarta(cartas: HashMap<Int, Carta>, cartaVirada: Carta, fiscos: Boolean = true): Int {
        val posJugadoresConFiscos = getPosFiscos(cartas, cartaVirada)

        when(posJugadoresConFiscos.size) {
            0 -> {
                return getPosMayorCarta(cartas, cartas[0]!!, false)
            }

            1 -> {
                return posJugadoresConFiscos[0]
            }

            else -> {
                val posFiscos = HashMap<Int, Carta>()

                for(pos in posJugadoresConFiscos)
                    posFiscos[pos] = cartas[pos]!!

                return getPosMayorNumero(posFiscos, fiscos)
            }
        }
    }

    /**
     * Método que comprueba cuál es el mayor triunfo
     *
     * @param cartas Hash con los números de los jugadores y el triunfo que ha jugado cada uno
     * @return Número del jugador que ha jugado el triunfo mayor
     */
    private fun getPosMayorTriunfo(cartas: HashMap<Int, Carta>): Int {
        var posJugadorGanador = cartas.keys.first()

        //Iteramos por todos los triunfos
        cartas.forEach{ pos, carta ->
            //Si la categoría del triunfo actual es mayor que el anterior, la guardamos como la posición ganadora actual
            if(getCategoriaTriunfo(carta) < getCategoriaTriunfo(cartas[posJugadorGanador]!!))
                posJugadorGanador = pos
        }

        return posJugadorGanador
    }

    /**
     * Método que comprueba cuál es la mayor carta
     *
     * @param cartas Hash con posición del jugador y su carta jugada
     * @param fiscos Si es true quiere decir que estamos tratando cartas del palo virado
     * @return Posición del jugador ganador de la mano
     */
    private fun getPosMayorNumero(cartas: HashMap<Int, Carta>, fiscos: Boolean): Int {
        var posMayorNumero = cartas.keys.first()
        var malilla = false

        cartas.forEach{ pos, carta ->
            //Si estamos tratando cartas del palo virado y hay un 2, este será el ganador
            if(fiscos && carta.numero == 2) {
                posMayorNumero = pos
                malilla = true
            }
            //Si aun no hemos encontrado malilla buscamos cuál es la mayor
            else if(!malilla) {
                //Si la carta actual es un 1 y la mayor actual es igual o menor de 7, el 1 será ganador
                if (cartas[posMayorNumero]!!.numero <= 7 && carta.numero == 1)
                    posMayorNumero = pos
                //Si no estamos en la situación de número mayor igual a 1 y número actual menor o igual a 7, compramos
                //si la actual es mayor que la gardada
                else if (!(cartas[posMayorNumero]!!.numero == 1 && carta.numero <= 7))
                //Si el número de la carta actual es mayor al guardado, actualizamos el mayor
                    if (carta.numero > cartas[posMayorNumero]!!.numero)
                        posMayorNumero = pos
            }
        }

        return posMayorNumero
    }

    /**
     * Método para obtener los números de los jugadores que han jugado algún triunfo en la mano actual
     *
     * @param cartas Cartas jugadas en esta mano
     * @return Array con las poisiciones de los jugadores que han jugado un triunfo en esta mano
     */
    private fun getPosTriunfos(cartas: HashMap<Int, Carta>): ArrayList<Int> {
        val posicionesTriunfos = ArrayList<Int>()

        cartas.forEach{ pos, carta ->
            if(isTriunfo(carta))
                posicionesTriunfos.add(pos)
        }

        return posicionesTriunfos
    }

    /**
     * Método para conocer el número de los jugadores que han jugado alguna carta de lo virado en la mano actual
     *
     * @param cartas Todas las cartas de la mano actual
     * @param cartaVirada Carta que está virada
     * @return Array con las posiciones de los jugadores que han jugado una carta del palo virado
     */
    private fun getPosFiscos(cartas: HashMap<Int, Carta>, cartaVirada: Carta): ArrayList<Int> {
        val posicionesFiscos = ArrayList<Int>()

        cartas.forEach{ pos, carta ->
            //Guardamos la posición del jugador si el palo de la carta jugada es igual al palo de la carta virada
            if(carta.palo == cartaVirada.palo)
                posicionesFiscos.add(pos)
        }

        return posicionesFiscos
    }

    /**
     * Método para obtener la categoría de un triunfo. Donde cuanto menor sea el número de la categoría, más fuerte es el triunfo
     *
     * @param carta Carta de la que queremos saber su categoría si esta es un triunfo
     * @return Número de la categoría en String
     */
    fun getCategoriaTriunfo(carta: Carta): String {
        val categoriaTriunfo = triunfos.count() - triunfos.indexOf(carta)

        return categoriaTriunfo.toString()
    }
}