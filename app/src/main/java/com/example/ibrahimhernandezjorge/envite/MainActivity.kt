package com.example.ibrahimhernandezjorge.envite

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.example.ibrahimhernandezjorge.envite.cartas.Baraja
import com.example.ibrahimhernandezjorge.envite.cartas.Carta
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {
    private var piedrasEquipo1: TextView? = null
    private var piedrasEquipo2: TextView? = null
    lateinit var jugadorIA: JugadorIA
    private var cartasJugadas = 0
    private var numeroManos = 0 //Lleva la cuenta de manos de una partida
    private var jugadorTurnoActual = 0 //Contendrá el turno del jugador que esta jugando en ese momento
    var numeroJugadores = 4
    private var jugadorBarajador = 3 //Número del jugador que baraja la partida, será el último en jugarla. 3 para que salga el humano
    private lateinit var baraja: Baraja
    lateinit var cartaVirada: Carta
    lateinit var cartasJugadores: ArrayList<ArrayList<Carta>>
    private lateinit var posCartaJugada: ArrayList<TextView>
    private lateinit var cartasManoActual: HashMap<Int, Carta> //Cartas que se han lanzado en la mano
    private lateinit var puntuacionEquipos:  ArrayList<Int>
    private var numeroCartasJugador = ArrayList<Int>()
    private var puntuacion = Puntuacion()
    private var arrastrando = false
    private var nuevaMano = false

    init {
        nuevaPartida()
    }

    fun nuevaPartida() {
        jugadorBarajador = jugadorBarajador.inc() % 4
        jugadorTurnoActual = jugadorBarajador
        numeroManos = 0
        cartasJugadas = 0
        baraja = Baraja(numeroJugadores)
        cartasJugadores = ArrayList()
        posCartaJugada = ArrayList()
        cartasManoActual = HashMap()
        puntuacionEquipos = ArrayList(numeroJugadores / 2)

        getCartaVirada()
        getCartasJugadores()
        initJugadorIA()
        initPuntuacionEquipos()
        initNumeroCartasJugadores()
    }

    private fun getCartaVirada() {
        cartaVirada = baraja.virarCarta()
    }

    private fun getCartasJugadores() {
        repeat(numeroJugadores) {
            val cartasJugador = baraja.getTresCartas()

            cartasJugadores.add(cartasJugador)
        }
    }

    private fun initPuntuacionEquipos() {
        puntuacionEquipos.add(0)
        puntuacionEquipos.add(0)
    }

    private fun initNumeroCartasJugadores() {
        for(i in 0 until numeroJugadores)
            numeroCartasJugador.add(3)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        piedrasEquipo1 = findViewById(R.id.piedrasEquipo1)
        piedrasEquipo2 = findViewById(R.id.piedrasEquipo2)

        initNumeroCartasJugadores()
        setImageViewsCartasMesa()
    }

    fun setImageViewsCartasMesa() {
        val idCartaVirada = resources.getIdentifier(cartaVirada.idImagen, "drawable", packageName)
        carta_virada.setImageResource(idCartaVirada)
        carta_virada.rotation = 90f

        for(numJugador in 0 until numeroJugadores)
            for(numCarta in 0 until 3) {
                val imageViewString = "jugador${numJugador + 1}_carta${numCarta + 1}"
                val imageCartaJugadaString = "carta_jugada${numJugador + 1}"

                val idImageView = resources.getIdentifier(imageViewString, "id", packageName)
                val idCarta = resources.getIdentifier(cartasJugadores[numJugador][numCarta].idImagen, "drawable", packageName)

                val imageViewjugador = findViewById<ImageView>(idImageView)
                imageViewjugador.visibility = View.VISIBLE
                imageViewjugador.setImageResource(idCarta)

                imageViewjugador.setOnClickListener {
                    if(jugadorTurnoActual == numJugador) {
                        jugarCarta(numJugador, numCarta)
                    }
                }

                rotarCarta(numJugador, imageViewjugador)
            }
    }

    private fun jugarCarta(numJugador: Int, numCarta: Int) {
        val imageViewString = "jugador${numJugador + 1}_carta${numCarta + 1}"
        val imageCartaJugadaString = "carta_jugada${numJugador + 1}"

        val idImageView = resources.getIdentifier(imageViewString, "id", packageName)
        val imageViewjugador = findViewById<ImageView>(idImageView)

        val idImageViewCartaJugada = resources.getIdentifier(imageCartaJugadaString, "id", packageName)
        val imageViewCartaJugada = findViewById<ImageView>(idImageViewCartaJugada)

        val cartaJugada = cartasJugadores[numJugador][numCarta]
        numeroCartasJugador[numJugador] = numeroCartasJugador[numJugador] - 1

        jugarCartaPulsadaInterfaz(cartaJugada, imageViewCartaJugada, imageViewjugador)
        //borrarCartaPulsada(numJugador, numCarta)
        rotarCarta(numJugador, imageViewCartaJugada)
    }

    private fun rotarCarta(numeroJugador: Int, imageViewCarta: ImageView) {
        when(numeroJugador) {
            1 -> imageViewCarta.rotation = 270f
            2 -> imageViewCarta.rotation = 180f
            3 -> imageViewCarta.rotation = -270f
        }
    }

    private fun jugarCartaPulsadaInterfaz(cartaPulsada: Carta, imageViewCartaJugada: ImageView, imageViewjugador: ImageView) {
        val cartasJugadorActual = cartasJugadores[jugadorTurnoActual]

        if(!arrastrando || cumpleArrastre(cartaPulsada, cartasJugadorActual)) {
            imageViewjugador.visibility = View.INVISIBLE
            cartasManoActual[cartasJugadas] = cartaPulsada

            val idCarta = resources.getIdentifier(cartaPulsada.idImagen, "drawable", packageName)
            imageViewCartaJugada.setImageResource(idCarta)

            nuevoTurno()
        }
        else
            Toast.makeText(this, "Hay que arrastrar", Toast.LENGTH_LONG).show()
    }

    private fun initJugadorIA() {
        val cartasJugadorIA = getCartasJugadorIA()

        jugadorIA = JugadorIA(cartasJugadorIA, baraja)
        jugadorIA.comprobarCartasInicio(cartaVirada)
    }

    private fun getCartasJugadorIA(): ArrayList<ArrayList<Carta>> {
        val cartasJugadorIA =  ArrayList<ArrayList<Carta>>()

        for(i in 0 until cartasJugadores.count())
            if(i % 2 != 0) {
                val arrayCartas = cartasJugadores[i]
                cartasJugadorIA.add(arrayCartas)
            }

        return cartasJugadorIA
    }

    private fun getCartasJugadorHumano(): ArrayList<ArrayList<Carta>> {
        val cartasJugadorHumano =  ArrayList<ArrayList<Carta>>()

        for(i in 0 until cartasJugadores.count())
            if(i % 2 == 0) {
                val arrayCartas = cartasJugadores[i]
                cartasJugadorHumano.add(arrayCartas)
            }

        return cartasJugadorHumano
    }

    fun nuevoTurno() {
        println("EMPIEZA EL TURNO")
        if(cartasJugadas == 0)
            comprobarArrastre()

        cartasJugadas = cartasJugadas.inc()
        jugadorTurnoActual = jugadorTurnoActual.inc()


        if(jugadorTurnoActual >= numeroJugadores)
            jugadorTurnoActual = 0

        if(cartasJugadas >= numeroJugadores) {
            arrastrando = false
            cartasJugadas = 0
            elegirGanadorMano()
        }

        println("NUMERO JUGADORACTUAL:  $jugadorTurnoActual")
        if(jugadorTurnoActual % 2 != 0) {
            println("JUEGA IA")
            getJugadaIA()
        }
        else {
            println("JUEGA USUARIO")
            //Juega el usuario
        }
    }

    fun getJugadaIA() {
        println("ANTES TURNOJUGADORIA:  TURNO JUGADOR ACTUAL: $jugadorTurnoActual")
        var turnoJugadorIA = 0

        //TODO Cambiar para tener en cuenta partidas de IA de más de 4 jugadores
        if(jugadorTurnoActual == 3)
            turnoJugadorIA = 1//jugadorTurnoActual % 1//numeroJugadores % cartasJugadas
        println("Turno Jugador IA: $turnoJugadorIA")
        val cartaJugada = jugadorIA.juegaCarta(numeroManos, turnoJugadorIA, cartasJugadas, getCartasJugadorHumano(), puntuacionEquipos[1], cartasManoActual)

        println("Carta jugada por el IA: $cartaJugada")

        var numeroCarta = -1
        for (cartasIA in jugadorIA.cartasEquipo)
            if(numeroCarta == -1)
                numeroCarta = cartasIA.indexOf(cartaJugada)

        println("NUMERO CARTA: $numeroCarta    NUMERO JUGADOR: $jugadorTurnoActual")
        jugarCarta(jugadorTurnoActual, numeroCarta)
        Thread.sleep(1500)
        //jugarCartaPulsada(cartaJugada!!, posCartaJugada[cartasJugadas])
    }

    private fun elegirGanadorMano() {
        //Obtenemos el turno de la carta que ha ganado
        val posicionCartaGanadora = baraja.getPosicionGanadora(cartasManoActual)
        //Convertimos el turno de la carta que ha ganado a número del jugador
        val jugadorGanador = getPosicionJugadorFromTurno(posicionCartaGanadora)
        cartasManoActual.clear()

        sumarPunto(jugadorGanador)

        if(!partidaGanada()) {
            //Incrementamos el número de manos jugadas
            numeroManos.inc()
            //Si la partida aun no esta ganada, se juega otra mano, donde comenzará sacando el jugador que ha ganado la mano anterior
            jugadorTurnoActual = jugadorGanador
            Toast.makeText(this, "Gana la mano el jugador ${jugadorGanador + 1}", Toast.LENGTH_LONG).show()
            nuevaMano = true
        }
        else {
            val equipoGanador = puntuacionEquipos.indexOf(2)

            Toast.makeText(this, "El equipo ${equipoGanador + 1} ha ganado la ronda!", Toast.LENGTH_LONG).show()
            ganaRonda(equipoGanador)
            reinicioJuego()
        }
    }

    /**
     * Método que convierte el número de turno relativo a número de jugador
     * @param turnoRelativo Posición de turno relativo
     */
    private fun getPosicionJugadorFromTurno(turnoRelativo: Int): Int {
        return (jugadorTurnoActual + turnoRelativo) % 4
    }

    private fun ganaRonda(numEquipo: Int) {
        puntuacion.addPiedras(numEquipo)

        val nuevaPuntuacion = "Piedras: ${puntuacion.piedras[numEquipo]}"
        getTextViewEquipoGanador(numEquipo)?.text = nuevaPuntuacion
    }

    private fun getTextViewEquipoGanador(numEquipo: Int): TextView? {
        return when (numEquipo) {
            0 -> piedrasEquipo1!!
            1 -> piedrasEquipo2!!
            else -> null
        }
    }

    private fun comprobarArrastre() {
        val cartaSacada = cartasManoActual[0]

        if(cartaSacada!!.palo == cartaVirada.palo || baraja.isTriunfo(cartaSacada))
            arrastrando = true
    }

    private fun cumpleArrastre(cartaJugada: Carta, cartasJugador: ArrayList<Carta>): Boolean {
        val paloArrastre = cartaVirada.palo

        if(cartaJugada.palo == paloArrastre || baraja.isTriunfo(cartaJugada) || numeroCartasJugador[jugadorTurnoActual] == 1) {
            return true
        }
        else {
            for(carta in cartasJugador) {
                if (carta.palo == paloArrastre || baraja.isTriunfo(carta)) {
                    return false
                }
            }
            return true
        }
    }

    /*private fun borrarCartaPulsada(numJugador: Int, numCarta: Int) {
        cartasJugadores[numJugador].removeAt(numCarta)
    }*/

    private fun setEnvido() {
        puntuacion.envido()
    }

    private fun addPuntos(numEquipo: Int, numPuntos: Int) {
        puntuacion.addPiedras(numEquipo, numPuntos)
    }

    private fun reinicioJuego() {
        nuevaPartida()
        setImageViewsCartasMesa()
    }

    private fun sumarPunto(jugadorGanador: Int) {
        puntuacionEquipos[jugadorGanador % 2] = puntuacionEquipos[jugadorGanador % 2].inc()
    }

    private fun partidaGanada(): Boolean {
        return puntuacionEquipos.contains(2)
    }
}
